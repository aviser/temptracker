#!/usr/bin/env bash
HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${HERE}"
git pull
python ./temptrack.py `hostname`