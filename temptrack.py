# coding=utf-8
import time, datetime
import json
import Adafruit_DHT as DHT
import paho.mqtt.publish as paho

# GPIO pin where the DHT sensor is connected
DHT_PIN = 4

SENSOR_CALIBRATION = {
    "pi1": ["Parents",  DHT.DHT11, DHT_PIN, +1.0, +12.0],
    "pi4": ["Basement", DHT.DHT22, DHT_PIN, +0.3, +07.0],
}

MQTT_BROKER="192.168.86.17"
MQTT_PORT=1883
MQTT_USER="homeassistant"
MQTT_PASS="8vm6e79sSiQ2MJX"

def create_ha_message(temperature, humidity):
    """Create a JSON message string for Home Assistant"""
    return json.dumps(
        {"temperature": temperature,
         "humidity": humidity
         })


def post_home_assistant(sensor_name, message):
    paho.single("tele/" + sensor_name,
                payload = message,
                qos=1,
                retain=True,
                hostname=MQTT_BROKER,
                port=MQTT_PORT,
                client_id=sensor_name,
                auth={'username': MQTT_USER, 'password': MQTT_PASS})

def read_and_post(host, sensors):
    temp = 20.0
    humidity = 40.0
    sensor_name = "test"
    if sensors:
        sensor_name, model, pin, calib_temp, calib_humidity = \
            SENSOR_CALIBRATION[host]
        humidity, temp = DHT.read_retry(model, pin)
        temp = "{:.1f}".format(temp + calib_temp)
        humidity = "{:.0f}".format(humidity + calib_humidity)

    print("  {} Host={} Name={} Temp={}*C  Humidity={}%".
          format(datetime.datetime.now(), host, sensor_name, temp, humidity))

    message= create_ha_message(temp, humidity)
    post_home_assistant(sensor_name, message)


def read_command_line():
    import argparse
    parser = argparse.ArgumentParser(description='Track temperature and humidity')
    parser.add_argument("host", choices=['pi1', 'pi4'],
                        help='Which host is writing')

    parser.add_argument('-n', action="store_const", const=False, default=True,
                        help='Flag indicates no sensors are connected to the Python VM')
    args = parser.parse_args()
    if not args.n:
        print("Not using sensors")
    return args.host, args.n


if __name__ == "__main__":
    host, sensors = read_command_line()
    read_and_post(host, sensors)